import React,{useCallback,useState,useEffect} from 'react';
import { StyleSheet, ScrollView,Text} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import {ListItem,Overlay,Button} from 'react-native-elements'
import Api from '../../utils/Api';
import Loader from '../../components/Loader';
import InfoCivi from '../../components/civilizations/InfoCivi';
const Civis = () => {
    const [civis, setcivis] = useState(null);
    const [showOver, setshowOver] = useState(false)
    const [idCivi, setidCivi] = useState(null)
    
    useEffect(() => {
        (async()=>{
            let api = new Api(`civilizations`,`GET`);
            await api.call()
            .then(res=>{
                setcivis(res.civilizations);
            });
        })()
    },[])
    
    let mostarInfoCivi  = (id) => {
        setshowOver(true)
        setidCivi(id)
    }

    return (
        !civis ? (
            <Loader/>
        ) : (
            <ScrollView>
                    {
                        civis.map((l, i) => (
                        <ListItem key={i} bottomDivider onPress = {()=>mostarInfoCivi(l.id)}>
                            {/* <Avatar source={{uri: l.avatar_url}} /> */}
                            <ListItem.Content>
                            <ListItem.Title>{l.name}</ListItem.Title>
                            <ListItem.Subtitle>{l.expansion}</ListItem.Subtitle>
                            </ListItem.Content>
                            <ListItem.Chevron />
                        </ListItem>
                        ))
                    }
                    <Overlay isVisible={showOver} onBackdropPress={ () => setshowOver(false)} >
                        <InfoCivi idCivi = {idCivi} Api = {Api} />
                        <Button onPress={()=>setshowOver(false)} title ="Cerrar" buttonStyle = { { backgroundColor:"black"}}/>
                    </Overlay>
            </ScrollView>
        )
    )
}
export default Civis

const styles = StyleSheet.create({
    body:{
        backgroundColor : '#fff',
        height : '100%'
    }
})
