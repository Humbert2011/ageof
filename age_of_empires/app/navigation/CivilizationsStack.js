import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Civis from "../screens/civilizations/Civis";

const Stack = createNativeStackNavigator();

const  CivilizationsStack = () => {
  return (
    // /<NavigationContainer>/
      <Stack.Navigator>
        <Stack.Screen 
          name= "home" 
          component={Civis} 
          options={{
            title:"Cvi",
            headerStyle:{
              backgroundColor:'#fff',
              elevation: 0,
              shadowOpacity: 0,
              borderBottomWidth: 0
            }
          }} />
      </Stack.Navigator>
    // </NavigationContainer>
  );
}

export default CivilizationsStack;