import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';

// imports locales

import Civis from '../screens/civilizations/Civis';
import Units from '../screens/units/Units';
import Structures from '../screens/structures/Structures';
import Technologies from '../screens/technologies/Technologies';

const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Civilizations" component={Civis} />
      <Drawer.Screen name="Units" component={Units} />
      <Drawer.Screen name="Structures" component={Structures} />
      <Drawer.Screen name="Technologies" component={Technologies} />
    </Drawer.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyDrawer />
    </NavigationContainer>
  );
}
