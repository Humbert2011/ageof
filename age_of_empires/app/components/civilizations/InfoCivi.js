import React,{useState} from 'react'
import { StyleSheet, View,ActivityIndicator } from 'react-native';
import {Text,Divider} from 'react-native-elements'
const InfoCivi = (props) => {
    const { idCivi,Api } = props;
    const [info, setinfo] = useState(null)
    let api = new Api(`civilization/${idCivi}`,`GET`);
        api.call()
        .then(res=>{
           setinfo(res)
        });
    return (
        !info ? (
            <ActivityIndicator size = "large" color = "black" />
        ): (
            <View>
                <Text h2>{info.name}</Text>
                <Divider orientation = "horizontal" style ={{marginTop:10}} />
                <Text h4>{info.expansion}</Text>
                <Text h4>{info.team_bonus}</Text>
                <Divider orientation = "horizontal" style = {{marginBottom:20,marginTop:10}}/>
            </View>
        )
    )
}

export default InfoCivi

const styles = StyleSheet.create({})
